package handlers

import (
	"fmt"
	"log"
	"net/http"
	"sort"

	"github.com/CloudyKit/jet/v6"
	"github.com/gorilla/websocket"
)

// channel with WsPayload as data type
var wsChan = make(chan WsPayload)

// the key is struct that has *websocket.Conn
var clients = make(map[WebSocketConnection]string)

var views = jet.NewSet(
	jet.NewOSFileSystemLoader("./html"), // it directs you to html dir
	jet.InDevelopmentMode(),
)

var upgradeConnection = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

// Home is an HTTP handler for the home route
func Home(w http.ResponseWriter, r *http.Request) {
	err := renderPage(w, "home.jet", nil)
	if err != nil {
		log.Println(err)
	}
}

type WebSocketConnection struct {
	*websocket.Conn
}

// WsJsonResponse defines the response sent back from websocket
type WsJsonResponse struct {
	Action         string   `json:"action"`
	Message        string   `json:"message"`
	MessageType    string   `json:"message_type"`
	ConnectedUsers []string `json:"connected_users"`
}

// Information that we are sending to the server
type WsPayload struct {
	Action   string              `json:"action"`
	Username string              `json:"username"`
	Message  string              `json:"message"`
	Conn     WebSocketConnection `json:"-"`
}

// WsEndpoint upgrades connection to websocket
func WsEndpoint(w http.ResponseWriter, r *http.Request) {
	ws, err := upgradeConnection.Upgrade(w, r, nil)
	// http.ResponseWriter => example: w.WriteHeader (status code), w.Header().Set (content type application/json)
	// http.Request => example: r.URL.Query().Get, r.Header.Get, r.URL.Path, r.UserAgent
	// http.Header => Header map[string][]string (the key is like Content-Type, User-Agent, etc.)
	if err != nil {
		log.Println(err)
	}

	log.Println("Client connected to endpoint")

	var response WsJsonResponse
	response.Message = `<em><small>Connected to server</small></em>`

	// filling data to struct, of course we pass ws because ws is *websocket.Conn
	conn := WebSocketConnection{Conn: ws}
	clients[conn] = ""

	// this ws is in *websocket.Conn type that we get from upgrading connection to websocket connection
	// this writeJSON is one of *websocket.Conn's method used to send JSON data to connected client
	err = ws.WriteJSON(response)
	if err != nil {
		log.Println(err)
	}

	go ListenForWs(&conn)
}

func ListenForWs(conn *WebSocketConnection) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("Error", fmt.Sprintf("%v", r))
		}
	}()

	var payload WsPayload

	for {
		// readJSON is reading from client
		err := conn.ReadJSON(&payload)
		if err != nil {
			// do nothing
		} else {
			payload.Conn = *conn
			wsChan <- payload
		}
	}
}

func ListenToWsChannel() {
	var response WsJsonResponse

	for {
		// wsChan and e have the same data type that is WsPayload
		// why? because the value is from err := conn.ReadJSON(&payload)
		e := <-wsChan

		// why we can use "username" as a case? because we already set it in js when we input a username
		// and it is sended to server as a payload, and then we store it in channel
		// so, we can use it in switch case like this
		switch e.Action {
		case "username":
			log.Println("client action: ", e.Action)

			// get a list of all users and send it back via broadcast
			clients[e.Conn] = e.Username
			users := getUserList()

			// filling response's data structure (only Action and ConnectedUsers)
			// after we read the payload, store it in channel, doing switch case, if it goes no problem,
			// then we can give response back to the client.
			// here we set the response's action as "list_users"
			response.Action = "list_users"
			log.Println("server action: ", response.Action)

			response.ConnectedUsers = users
			broadcastToAll(response)
			log.Println(response)

		case "left":
			log.Println("client action: ", e.Action)

			response.Action = "list_users"
			log.Println("server action: ", response.Action)

			delete(clients, e.Conn)
			users := getUserList()
			response.ConnectedUsers = users
			broadcastToAll(response)

		case "broadcast":
			response.Action = "broadcast"
			response.Message = fmt.Sprintf("<strong>%s</strong>: %s", e.Username, e.Message)
			broadcastToAll(response)
		}

		// response.Action = "Got here"
		// response.Message = fmt.Sprintf("Some message, and action was %s", e.Action)
		// broadcastToAll(response)
	}
}

func getUserList() []string {
	var userList []string
	for _, x := range clients {
		if x != "" {
			userList = append(userList, x)
		}
	}
	sort.Strings(userList)
	return userList
}

func broadcastToAll(response WsJsonResponse) {
	for client := range clients {
		// writeJSON is writing/sending to client
		err := client.WriteJSON(response)
		if err != nil {
			log.Println("websocket err")
			_ = client.Close()
			delete(clients, client)
		}
	}
}

// we can actually build a html template and render it with html/template
// and the step is actually not too different. We get the template and execute it.
func renderPage(w http.ResponseWriter, tmpl string, data jet.VarMap) error {
	view, err := views.GetTemplate(tmpl)
	if err != nil {
		log.Println("cannot get template")
		log.Println(err)
		return err
	}

	err = view.Execute(w, data, nil)
	if err != nil {
		log.Println("cannot execute")
		log.Println(err)
		return err
	}

	return nil
}
